package amiciandoskar.jsnorway;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Emulius on 23/12/2016.
 */

public class JSNorwaySQLHelper extends SQLiteOpenHelper
{
    private static final String TAG = "SQLiteOpenHelper";
    private static final String DB_NAME = "JSNorway_db";
    private static final String DB_PATH = "JSNorway.db";
    private Context contextForDB;
    private SQLiteDatabase readableDB;

    private boolean createDb = false, upgradeDb = false;

    JSNorwaySQLHelper(Context context)
    {
        super(context, DB_NAME, null, 1);
        contextForDB = context;
    }

    private void copyDatabaseFromAssets(SQLiteDatabase db)
    {
        Log.i(TAG, "copyDatabase");
        InputStream myInput = null;
        OutputStream myOutput = null;
        try
        {
            // Open db packaged as asset as the input stream
            myInput = contextForDB.getAssets().open(DB_PATH);

            // Open the db in the application package context:
            myOutput = new FileOutputStream(db.getPath());

            // Transfer db file contents:
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0)
            {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();

            // Set the version of the copied database to the current version:
            SQLiteDatabase copiedDb = contextForDB.openOrCreateDatabase(DB_NAME, 0, null);
            copiedDb.execSQL("PRAGMA user_version = " + 1);
            copiedDb.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new Error(TAG + " Error copying database");
        }
        finally
        {
            // Close the streams
            try {
                if (myOutput != null)
                {
                    myOutput.close();
                }
                if (myInput != null)
                {
                    myInput.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new Error(TAG + " Error closing streams");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.i(TAG, "onCreate db");
        createDb = true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1)
    {
        Log.i(TAG, "onUpgrade db");
        upgradeDb = true;
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        Log.i(TAG, "onOpen db");
        if (createDb)
        {
            // The db in the application package context is being created.
            // So copy the contents from the db file packaged in the assets folder:
            createDb = false;
            copyDatabaseFromAssets(db);
        }
        if (upgradeDb)
        {
            // The db in the application package context is
            // being upgraded from a lower to a higher version.
            upgradeDb = false;
            // Your db upgrade logic here:
        }
    }

    public class ProgressStage
    {
        public int level;
        public int experienceToLevelUp;
        public String title;
        public String eng_subtext;
        public String pers_subtext;
        public String arab_subtext;

        ProgressStage(int theLevel, int theExperienceToLevelUp, String theTitleOfLevel,
                      String theEngSub, String thePersSub, String theArabSub)
        {
            level = theLevel;
            experienceToLevelUp = theExperienceToLevelUp;
            title = theTitleOfLevel;

            eng_subtext = theEngSub;
            pers_subtext = thePersSub;
            arab_subtext = theArabSub;
        }
    }

    public class LanguageItem
    {
        public int languageItemID;
        public String primarySentence;
        public String alternativeSentence;

        public String englishTranslation;
        public String persianTranslation;
        public String arabicTranslation;

        ExplanationByWord explanationByWord;
        ExtraExplanation extraExplanation;

        MissionsList missionsList;

        LanguageItem(int itemID, String primary, String alternative,
                     String english, String persian, String arabic,
                     ExplanationByWord byWord, ExtraExplanation extra, MissionsList list)
        {
            languageItemID = itemID;
            primarySentence = primary;
            alternativeSentence = alternative;
            englishTranslation = english;
            persianTranslation = persian;
            arabicTranslation = arabic;

            explanationByWord = byWord;
            extraExplanation = extra;
            missionsList = list;
        }
    }

    public class ExplanationByWord
    {
        public String[] norwegianWords;
        public String[] englishWordMeaning;
        public String[] persianWordMeaning;
        public String[] arabicWordMeaning;

        public String[] englishWordFunction;
        public String[] persianWordFunction;
        public String[] arabicWordFunction;

        ExplanationByWord(String[] norwegian, String[] englishMeaning, String[] persianMeaning,
                          String[] arabicMeaning, String[] englishFunction,
                          String[] persianFunction, String[] arabicFunction)
        {
            norwegianWords = norwegian;
            englishWordMeaning = englishMeaning;
            persianWordMeaning = persianMeaning;
            arabicWordMeaning = arabicMeaning;

            englishWordFunction = englishFunction;
            persianWordFunction = persianFunction;
            arabicWordFunction = arabicFunction;
        }

        public int getWordCount()
        {
            return norwegianWords.length;
        }
    }

    public class ExtraExplanation
    {
        public String[] norwegianWords;

        public String[] englishWordClass;
        public String[] persianWordClass;
        public String[] arabicWordClass;

        public String[] englishWordNotes;
        public String[] persianWordNotes;
        public String[] arabicWordNotes;

        ExtraExplanation(String[] norwegian, String[] englishClass, String[] persianClass,
                         String[] arabicClass, String[] englishNotes, String[] persianNotes,
                         String[] arabicNotes)
        {
            norwegianWords = norwegian;
            englishWordClass = englishClass;
            persianWordClass = persianClass;
            arabicWordClass = arabicClass;
            englishWordNotes = englishNotes;
            persianWordNotes = persianNotes;
            arabicWordNotes = arabicNotes;
        }

        public int getWordCount()
        {
            return norwegianWords.length;
        }
    }

    public class MissionsList
    {
        public Mission[] missions;

        MissionsList (Mission[] theMissions)
        {
            missions = theMissions;
        }

        public int getNumberOfMissions()
        {
            return missions.length;
        }
    }

    public class Mission
    {
        public int missionNumber;   // unique number for each individual mission

        public String engTypeOfMission;
        public String persTypeOfMission;
        public String arabTypeOfMission;

        public String englishDescription;
        public String persianDescription;
        public String arabicDescription;

        public int pointsWorth;

        Mission (int theMissionNumber, String theEngTypeOfMission, String thePersTypeOfMission, String theArabTypeOfMission,
                 String theEnglishDescription, String thePersianDescription, String theArabicDescription, int thePointsWorth)
        {
            missionNumber = theMissionNumber;
            engTypeOfMission = theEngTypeOfMission;
            persTypeOfMission = thePersTypeOfMission;
            arabTypeOfMission = theArabTypeOfMission;
            englishDescription = theEnglishDescription;
            persianDescription = thePersianDescription;
            arabicDescription = theArabicDescription;
            pointsWorth = thePointsWorth;
        }
    }

    private Cursor getTable(String tableName, String[] columns, String where, String orderBy)
    {
        readableDB = getReadableDatabase();

        return readableDB.query(tableName, columns, where, null, null, null, orderBy);
    }

    public LanguageItem getLanguageItem (int theLanguageItemID)
    {
        final int NUM_OF_MISSIONS_PER_ITEM = 9;

        ExplanationByWord explanationByWord;
        ExtraExplanation extraExplanation;
        MissionsList missionsList;

        Cursor currentTable;
        int numOfRows;
        String currentLanguageItem = "id=" + Integer.toString(theLanguageItemID);

        //region Language Item
        currentTable = getTable("language_item", null, currentLanguageItem, "id ASC");
        currentTable.moveToFirst();

        int languageItemID = currentTable.getInt(0);
        String primarySentence = currentTable.getString(1);
        String alternativeSentence = currentTable.getString(2);
        //endregion

        //region Sentence Translations
        currentTable = getTable("sentence_translations", null, currentLanguageItem, "id ASC");
        currentTable.moveToFirst();

        String englishTranslation = currentTable.getString(1);
        String persianTranslation = currentTable.getString(2);
        String arabicTranslation = currentTable.getString(3);
        //endregion

        //region Explanation By Word
        currentTable = getTable("explanation_by_word", null, currentLanguageItem, "word_number ASC");
        currentTable.moveToFirst();

        numOfRows = currentTable.getCount();

        String[] norwegianWords = new String[numOfRows];

        String[] englishWordMeaning = new String[numOfRows];;
        String[] persianWordMeaning = new String[numOfRows];;
        String[] arabicWordMeaning = new String[numOfRows];;

        String[] englishWordFunction = new String[numOfRows];;
        String[] persianWordFunction = new String[numOfRows];;
        String[] arabicWordFunction = new String[numOfRows];;

        for (int i = 0; i < numOfRows; i++)
        {
            norwegianWords[i] = currentTable.getString(2);
            englishWordMeaning[i] = currentTable.getString(3);
            persianWordMeaning[i] = currentTable.getString(4);
            arabicWordMeaning[i] = currentTable.getString(5);
            englishWordFunction[i] = currentTable.getString(6);
            persianWordFunction[i] = currentTable.getString(7);
            arabicWordFunction[i] = currentTable.getString(8);

            currentTable.moveToNext();
        }

        explanationByWord = new ExplanationByWord (norwegianWords, englishWordMeaning,
                persianWordMeaning, arabicWordMeaning, englishWordFunction,
                persianWordFunction, arabicWordFunction);
        //endregion

        //region Extra Explanation
        currentTable = getTable("extra_explanation", null, currentLanguageItem, "word_number ASC");
        currentTable.moveToFirst();

        numOfRows = currentTable.getCount();

        String[] norwegianWords2 = new String[numOfRows];

        String[] englishWordClass = new String[numOfRows];
        String[] persianWordClass = new String[numOfRows];
        String[] arabicWordClass = new String[numOfRows];

        String[] englishWordNotes = new String[numOfRows];
        String[] persianWordNotes = new String[numOfRows];
        String[] arabicWordNotes = new String[numOfRows];

        for (int i = 0; i < numOfRows; i++)
        {
            norwegianWords2[i] = currentTable.getString(2);
            englishWordClass[i] = currentTable.getString(3);
            persianWordClass[i] = currentTable.getString(4);
            arabicWordClass[i] = currentTable.getString(5);
            englishWordNotes[i] = currentTable.getString(6);
            persianWordNotes[i] = currentTable.getString(7);
            arabicWordNotes[i] = currentTable.getString(8);

            currentTable.moveToNext();
        }

        extraExplanation = new ExtraExplanation(norwegianWords2, englishWordClass,
                persianWordClass, arabicWordClass, englishWordNotes, persianWordNotes,
                arabicWordNotes);
        //endregion

        //region Missions List
        currentTable = getTable("missions", null, currentLanguageItem, "mission_number ASC");
        currentTable.moveToFirst();

        numOfRows = currentTable.getCount();

        Mission[] missions = new Mission[NUM_OF_MISSIONS_PER_ITEM];

        int missionNumber;

        String type_eng;
        String type_pers;
        String type_arab;

        String eng_description;
        String pers_description;
        String arab_description;

        int points = 0;

        for (int i = 0; i < missions.length; i++)
        {
            missionNumber = currentTable.getInt(1);

            type_eng = currentTable.getString(2);
            type_pers = currentTable.getString(3);
            type_arab = currentTable.getString(4);

            eng_description = currentTable.getString(5);
            pers_description = currentTable.getString(6);
            arab_description = currentTable.getString(7);

            points = currentTable.getInt(8);

            missions[i] = new Mission(missionNumber, type_eng, type_pers, type_arab,
                    eng_description, pers_description, arab_description, points);

            currentTable.moveToNext();
        }

        missionsList = new MissionsList(missions);
        //endregion

        return new LanguageItem(languageItemID, primarySentence, alternativeSentence,
                englishTranslation, persianTranslation, arabicTranslation,
                explanationByWord, extraExplanation, missionsList);
    }

    public ProgressStage getProgressStage (int level)
    {
        Cursor table;
        String str_level = "id=" + Integer.toString(level);

        table = getTable("progress_path", null, str_level, "level ASC");
        table.moveToFirst();

        int DB_level = table.getInt(0);
        int exp = table.getInt(1);

        String title = table.getString(2);
        String eng_subtext = table.getString(3);
        String pers_subtext = table.getString(4);
        String arab_subtext = table.getString(5);

        return new ProgressStage(DB_level, exp, title, eng_subtext, pers_subtext, arab_subtext);
    }

    public int getNumOfLanguageItems()
    {
        return getTable("language_item", new String[]{ "id" }, null, null).getCount();
    }
}
