package amiciandoskar.jsnorway;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity
{
    Button english, persian, arabic, info;
    Button learn_language, know_norway, language_roulette, reset_known;
    Button quicklist, about, progress;

    SharedPreferences app_preferences;
    SharedPreferences.Editor preferences_editor;
    int some_language;
    float screenSizeModifier;

    DisplayMetrics metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region Sets up settings functionality
        app_preferences = getSharedPreferences("app_settings", 0);
        preferences_editor = app_preferences.edit();
        //endregion

        //region Shared app language code
        if (app_preferences.contains("language_setting"))
        {
            some_language = app_preferences.getInt("language_setting", 0);
        } else
        {
            preferences_editor.clear();
            preferences_editor.putInt("language_setting", 0);
            preferences_editor.apply();
        }
        //endregion

        // sets up language buttons
        english = (Button) findViewById(R.id.btn_english);
        persian = (Button) findViewById(R.id.btn_persian);
        arabic = (Button) findViewById(R.id.btn_arabic);
        info = (Button) findViewById(R.id.btn_info);

        // sets up primary buttons
        learn_language = (Button) findViewById(R.id.btn_learn_language);
        know_norway = (Button) findViewById(R.id.btn_know_norway);
        language_roulette = (Button) findViewById(R.id.btn_language_roulette);
        reset_known = (Button) findViewById(R.id.btn_reset_known);

        // sets up secondary buttons
        quicklist = (Button) findViewById(R.id.btn_quicklist);
        about = (Button) findViewById(R.id.btn_about);
        progress = (Button) findViewById(R.id.btn_progress);

        // apply language settings
        setLanguage(some_language);

        initializeModifier();
        modifyScreen();
    }

    private void initializeModifier()
    {
        metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        screenSizeModifier = 0.001F * metrics.widthPixels;
    }

    // S.M.V. = screenModifiedValue
    private int SMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier);
    }

    // S.M.V. = screenModifiedValue
    private int TextSMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier / metrics.scaledDensity);
    }

    // S.M.V. = screenModifiedValue
    private float SMV(float originalValue)
    {
        return originalValue * screenSizeModifier;
    }
    
    // modifies the screen elements' distance and size values based on screen size in width
    private void modifyScreen()
    {
        //region General look attributes for buttons
        LinearLayout.LayoutParams btnLayoutParams;
        btnLayoutParams = new LinearLayout.LayoutParams(SMV(300), SMV(155));
        int langBtnPadding = SMV(30);
        int langBtnTextSize = TextSMV(40);

        //region Language Buttons + Info
        LinearLayout.LayoutParams languageBtnLayoutParamsTop;
        languageBtnLayoutParamsTop = new LinearLayout.LayoutParams(SMV(250), SMV(155));

        english.setLayoutParams(languageBtnLayoutParamsTop);
        english.setTextSize(langBtnTextSize);
        english.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        persian.setLayoutParams(languageBtnLayoutParamsTop);
        persian.setTextSize(langBtnTextSize);
        persian.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        arabic.setLayoutParams(languageBtnLayoutParamsTop);
        arabic.setTextSize(langBtnTextSize);
        arabic.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        info.setLayoutParams(new LinearLayout.LayoutParams(SMV(150), SMV(155)));
        info.setTextSize(langBtnTextSize);
        info.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);
        //endregion

        //region Center Buttons
        LinearLayout.LayoutParams centerBtnLayoutParams;
        centerBtnLayoutParams = new LinearLayout.LayoutParams(SMV(425), SMV(195));
        centerBtnLayoutParams.setMargins(0, SMV(15), 0, SMV(15));

        learn_language.setLayoutParams(centerBtnLayoutParams);
        learn_language.setTextSize(langBtnTextSize);
        learn_language.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        know_norway.setLayoutParams(centerBtnLayoutParams);
        know_norway.setTextSize(langBtnTextSize);
        know_norway.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        language_roulette.setLayoutParams(centerBtnLayoutParams);
        language_roulette.setTextSize(langBtnTextSize);
        language_roulette.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        reset_known.setLayoutParams(centerBtnLayoutParams);
        reset_known.setTextSize(langBtnTextSize);
        reset_known.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);
        //endregion

        //region Bottom Buttons
        quicklist.setLayoutParams(btnLayoutParams);
        quicklist.setTextSize(langBtnTextSize);
        quicklist.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        about.setLayoutParams(btnLayoutParams);
        about.setTextSize(langBtnTextSize);
        about.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        progress.setLayoutParams(btnLayoutParams);
        progress.setTextSize(langBtnTextSize);
        progress.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);
        //endregion
    }

    private void setLanguage(int lang)
    {
        switch (lang)
        {
            case 0:
                learn_language.setText(R.string.eng_setText_learn_language);
                know_norway.setText(R.string.eng_setText_know_norway);
                language_roulette.setText(R.string.eng_setText_language_roulette);
                quicklist.setText(R.string.eng_setText_quicklist);
                about.setText(R.string.eng_setText_about);
                progress.setText(R.string.eng_setText_progess);
                break;
            case 1:
                learn_language.setText(R.string.pers_setText_learn_language);
                know_norway.setText(R.string.pers_setText_know_norway);
                language_roulette.setText(R.string.pers_setText_language_roulette);
                quicklist.setText(R.string.pers_setText_quicklist);
                about.setText(R.string.pers_setText_about);
                progress.setText(R.string.pers_setText_progess);
                break;
            case 2:
                learn_language.setText(R.string.arab_setText_learn_language);
                know_norway.setText(R.string.arab_setText_know_norway);
                language_roulette.setText(R.string.arab_setText_language_roulette);
                quicklist.setText(R.string.arab_setText_quicklist);
                about.setText(R.string.arab_setText_about);
                progress.setText(R.string.arab_setText_progess);
                break;
        }
    }

    public void makeEnglish(View view)
    {
        setLanguage(0);
        preferences_editor.putInt("language_setting", 0);
        preferences_editor.apply();
    }

    public void makePersian(View view)
    {
        setLanguage(1);
        preferences_editor.putInt("language_setting", 1);
        preferences_editor.apply();
    }

    public void makeArabic(View view)
    {
        setLanguage(2);
        preferences_editor.putInt("language_setting", 2);
        preferences_editor.apply();
    }

    // actions for primary buttons
    public void goto_Learn_Language(View view)
    {
        Intent lang_intent = new Intent(this, amiciandoskar.jsnorway.language_learning.class);
        lang_intent.putExtra("amiciandoskar.jsnorway.CURRENT_LANG", some_language);
        startActivity(lang_intent);
    }

    public void goto_Know_Norway(View view)
    {

    }

    public void goto_Language_Roulette(View view)
    {

    }

    public void reset_known(View view)
    {
        JSNorwaySQLHelper helper = new JSNorwaySQLHelper(this);
        int LAST_LANGUAGE_ITEM = helper.getNumOfLanguageItems();

        final String txt = "knowNumber";
        int language_item = 1;
        String number = Integer.toString(language_item);

        for (int i = 1; i <= LAST_LANGUAGE_ITEM; i++)
        {
            preferences_editor.putBoolean(txt + number, false);
        }
    }

    // actions for secondary buttons
    public void goto_Quicklist(View view)
    {

    }

    public void goto_About(View view)
    {

    }

    public void goto_Progress(View view)
    {

    }
}