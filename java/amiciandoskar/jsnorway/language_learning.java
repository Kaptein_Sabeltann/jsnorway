package amiciandoskar.jsnorway;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.String.valueOf;

public class language_learning extends AppCompatActivity
{
    Button english, persian, arabic, info;
    TextView language_item_num;
    ImageButton forward, backward, skip_forward, skip_backward;
    Button goToMissions;
    Button alternative, listen;

    TextView norwegian_sentence, other_information;
    TextView showing_sentence, showing_information;
    TableLayout table;

    Button translation, explanation_by_word, extra_explanation, sentence_structure;

    SharedPreferences app_preferences;
    SharedPreferences.Editor preferences_editor;

    int some_language;
    int index_current_language_item;
    final int FIRST_LANGUAGE_ITEM = 1;
    int LAST_LANGUAGE_ITEM;     // initialized with helper.getNumOfLanguageItems()
    final int SKIP_QUANTITY = 3;
    int sentenceAudioID;

    float screenSizeModifier;     // initialized with initializeModifier()

    // 0 = translation
    // 1 = explanation by word
    // 2 = extra explanation
    // 3 = sentence_structure
    int showing_information_type = 0;

    boolean toggleAlternativeSentence = false;

    DisplayMetrics metrics;

    JSNorwaySQLHelper helper = new JSNorwaySQLHelper(this);

    JSNorwaySQLHelper.LanguageItem current_language_item;

    //region GUI Strings
    // GUI String variables
    String str_primary;
    String str_alt;
    String str_translation;
    String str_explanation;
    String str_extra;
    String str_structure;

    // English GUI Strings
    static final String eng_str_primary = "Primary Sentence to Learn";
    static final String eng_str_alt = "Alternative Sentence";
    static final String eng_str_translation = "Showing Translation";
    static final String eng_str_explanation = "Showing Explanation by Word";
    static final String eng_str_extra = "Showing Extra Explanation";
    static final String eng_str_structure = "Showing Sentence Structure";

    // Persian GUI Strings
    static final String pers_str_primary = "حکم اولیه برای یادگیری";
    static final String pers_str_alt = "حکم جایگزین";
    static final String pers_str_translation = "نمایش ترجمه";
    static final String pers_str_explanation = "نمایش توضیحات ورد";
    static final String pers_str_extra = "نمایش توضیح اضافی";
    static final String pers_str_structure = "نمایش ساختار جمله";

    // Arabic GUI Strings
    static final String arab_str_primary = "الجملة الابتدائية لتعلم";
    static final String arab_str_alt = "الجملة البديلة";
    static final String arab_str_translation = "عرض الترجمة";
    static final String arab_str_explanation = "عرض الشرح عن طريق الكلمة";
    static final String arab_str_extra = "عرض شرح إضافي";
    static final String arab_str_structure = "عرض بناء الجملة";
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_learning);

        //region Sets up settings functionality
        app_preferences = getSharedPreferences("app_settings", 0);
        preferences_editor = app_preferences.edit();
        //endregion

        some_language = getIntent().getIntExtra("amiciandoskar.jsnorway.CURRENT_LANG", 0);

        //region Retrieves last used sentence, for user convenience
        if (app_preferences.contains("last_sentence_used"))
        {
            index_current_language_item = app_preferences.getInt("last_sentence_used", 1);
        } else
        {
            preferences_editor.clear();
            preferences_editor.putInt("last_sentence_used", 1);
            preferences_editor.apply();
            index_current_language_item = 1;
        }
        //endregion
        //endregion

        //region Retrieving views
        // sets up language buttons and info-button
        english = (Button) findViewById(R.id.btn_english);
        persian = (Button) findViewById(R.id.btn_persian);
        arabic = (Button) findViewById(R.id.btn_arabic);
        info = (Button) findViewById(R.id.btn_info);

        // set up textview for language item number
        language_item_num = (TextView) findViewById(R.id.txt_language_item_num);

        // set up navigation buttons
        forward = (ImageButton) findViewById(R.id.btn_forward);
        backward = (ImageButton) findViewById(R.id.btn_backward);
        skip_forward = (ImageButton) findViewById(R.id.btn_skipForward);
        skip_backward = (ImageButton) findViewById(R.id.btn_skipBackward);

        // set up button for opening mission activity
        goToMissions = (Button) findViewById(R.id.btn_go_to_missions);

        // set up learning aid buttons
        alternative = (Button) findViewById(R.id.btn_alternative);
        listen = (Button) findViewById(R.id.btn_listen);

        // set up textviews for sentence and information display
        norwegian_sentence = (TextView) findViewById(R.id.txt_norwegian_sentence);
        other_information = (TextView) findViewById(R.id.txt_other_information);
        showing_sentence = (TextView) findViewById(R.id.txt_showing_sentence);
        showing_information = (TextView) findViewById(R.id.txt_showing_information);
        table = (TableLayout) findViewById(R.id.table);

        // set up buttons for information changing
        translation = (Button) findViewById(R.id.btn_translation);
        explanation_by_word = (Button) findViewById(R.id.btn_explanation_by_word);
        extra_explanation = (Button) findViewById(R.id.btn_extra_explanation);
        sentence_structure = (Button) findViewById(R.id.btn_sentence_structure);
        //endregion

        LAST_LANGUAGE_ITEM = helper.getNumOfLanguageItems();
        changeLanguageStrings(some_language);
        updateToNewSentence();
        displayTranslation(null);
        initializeModifier();
        modifyScreen();
    }

    private void initializeModifier()
    {
        metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        screenSizeModifier = 0.001F * metrics.widthPixels;
    }

    // S.M.V. = screenModifiedValue
    private int SMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier);
    }

    // S.M.V. = screenModifiedValue
    private int TextSMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier / metrics.scaledDensity);
    }

    // S.M.V. = screenModifiedValue
    private float SMV(float originalValue)
    {
        return originalValue * screenSizeModifier;
    }

    // modifies the screen elements' distance and size values based on screen size in width
    private void modifyScreen()
    {
        //region Language Buttons + Info
        LinearLayout.LayoutParams languageBtnLayoutParams;
        languageBtnLayoutParams = new LinearLayout.LayoutParams(SMV(250), SMV(150));
        int langBtnPadding = SMV(40);
        int langBtnTextSize = TextSMV(40);

        english.setLayoutParams(languageBtnLayoutParams);
        english.setTextSize(langBtnTextSize);
        english.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        persian.setLayoutParams(languageBtnLayoutParams);
        persian.setTextSize(langBtnTextSize);
        persian.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        arabic.setLayoutParams(languageBtnLayoutParams);
        arabic.setTextSize(langBtnTextSize);
        arabic.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        info.setLayoutParams(new LinearLayout.LayoutParams(SMV(150), SMV(150)));
        info.setTextSize(langBtnTextSize);
        info.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);
        //endregion

        //region Upper Buttons
        
        //endregion

        //region Lower Buttons

        //endregion
    }

    private void updateToNewSentence()
    {
        isOtherInformationTable(false);

        language_item_num.setText(valueOf(index_current_language_item) + "/" + valueOf(LAST_LANGUAGE_ITEM));

        current_language_item = helper.getLanguageItem(index_current_language_item);

        // Sets sentence to new primary sentence by default
        norwegian_sentence.setText(current_language_item.primarySentence);
        showing_sentence.setText(str_primary);

        // Sets correct translation by default
        switch (some_language)
        {
            case 0:
                other_information.setText(current_language_item.englishTranslation);
                break;
            case 1:
                other_information.setText(current_language_item.persianTranslation);
                break;
            case 2:
                other_information.setText(current_language_item.arabicTranslation);
                break;
        }

        loadPronounciation();
    }

    // Used to enable or disable views depending on text requirements
    private void isOtherInformationTable(boolean isTable)
    {
        table.removeAllViews(); // cleans TableLayout to accomodate new information

        // if table, then show, else hide completely
        table.setVisibility((isTable) ? View.VISIBLE : View.GONE);

        // if table, hide completely, else show
        other_information.setVisibility( (isTable) ? View.GONE : View.VISIBLE );
     }

    private void changeToNotKnown(int amountToChange)
    {
        final String txt = "knowNumber";
        String number;

        int knownCounted = 0;
        Boolean temp;

        do
        {
            if (index_current_language_item + amountToChange > LAST_LANGUAGE_ITEM)
            {
                index_current_language_item = FIRST_LANGUAGE_ITEM;
            }
            else if (index_current_language_item + amountToChange < FIRST_LANGUAGE_ITEM)
            {
                index_current_language_item = LAST_LANGUAGE_ITEM;
            }
            else
            {
                index_current_language_item += amountToChange;
            }

            knownCounted++;

            number = Integer.toString(index_current_language_item);
            temp = app_preferences.getBoolean(txt + number, false);

            if (knownCounted == LAST_LANGUAGE_ITEM - 1)
            {
                temp = false;
                index_current_language_item = 1;
            }
        } while (temp);
    }

    public void goToNextSentence(View view)
    {
        isOtherInformationTable(false);

        final int amountToChange = 1;

        changeToNotKnown(amountToChange);
        updateToNewSentence();
    }

    public void goToPreviousSentence(View view)
    {
        isOtherInformationTable(false);

        final int amountToChange = -1;

        changeToNotKnown(amountToChange);
        updateToNewSentence();
    }

    public void skipToLaterSentence(View view)
    {
        isOtherInformationTable(false);

        final int amountToChange = SKIP_QUANTITY;

        changeToNotKnown(amountToChange);
        updateToNewSentence();
    }

    public void skipToEarlierSentence(View view)
    {
        isOtherInformationTable(false);

        final int amountToChange = -SKIP_QUANTITY;

        changeToNotKnown(amountToChange);
        updateToNewSentence();
    }

    // alternates between original and alternative norwegian sentence
    public void changeSentenceVersion(View view)
    {
        toggleAlternativeSentence = !toggleAlternativeSentence;

        String sentenceToApply = (toggleAlternativeSentence) ?
                current_language_item.alternativeSentence :
                current_language_item.primarySentence;
        norwegian_sentence.setText(sentenceToApply);

        String sentenceReminderToApply = (toggleAlternativeSentence) ?
                str_alt : str_primary;
        showing_sentence.setText(sentenceReminderToApply);

        String buttonTextToDisplay = (toggleAlternativeSentence) ?
                "original" : "alternative";
        alternative.setText(buttonTextToDisplay);

        loadPronounciation();
    }

    private void loadPronounciation()
    {
        String sentenceAudioName = (toggleAlternativeSentence) ? "alt_sentence" : "sentence";
        sentenceAudioName += valueOf(index_current_language_item);  // adds index to file name string
        sentenceAudioID = getResources().getIdentifier(sentenceAudioName, "raw", getPackageName());

        Boolean thereIsAudio = (sentenceAudioID != 0);


        // signifies whether audio is available or not
        float alphaToApply = (thereIsAudio) ? 1 : 0.8F;
        listen.setAlpha(alphaToApply);
        listen.setClickable(thereIsAudio);
    }

    public void playPronounciation(View view)
    {
        MediaPlayer sentenceAudio = MediaPlayer.create(getApplicationContext(), sentenceAudioID);
        sentenceAudio.start();
    }

    public void displayTranslation(View view)
    {
        showing_information.setText(str_translation);
        showing_information_type = 0;

        isOtherInformationTable(false);

        // Sets correct translation by default
        switch (some_language)
        {
            case 0:
                other_information.setText(current_language_item.englishTranslation);
                break;
            case 1:
                other_information.setText(current_language_item.persianTranslation);
                break;
            case 2:
                other_information.setText(current_language_item.arabicTranslation);
                break;
        }
    }
    public void displayExplanationByWord(View view)
    {
        showing_information.setText(str_explanation);
        showing_information_type = 1;

        isOtherInformationTable(true);

        JSNorwaySQLHelper.ExplanationByWord explanation = current_language_item.explanationByWord;

        final String TITLE_CELL1 = "WORD";
        final String TITLE_CELL2 = "MEANING";
        final String TITLE_CELL3 = "FUNCTION";

        String[][] content = new String[][]
                {
                        explanation.norwegianWords,
                        explanation.englishWordMeaning, explanation.englishWordFunction,
                        explanation.persianWordMeaning, explanation.persianWordFunction,
                        explanation.arabicWordMeaning, explanation.arabicWordFunction
                };

        makeTable(explanation.getWordCount(), TITLE_CELL1, TITLE_CELL2, TITLE_CELL3, content);
    }

    public void displayExtraExplanation(View view)
    {
        showing_information.setText(str_extra);
        showing_information_type = 2;

        isOtherInformationTable(true);

        JSNorwaySQLHelper.ExtraExplanation extra = current_language_item.extraExplanation;

        final String TITLE_CELL1 = "WORD";
        final String TITLE_CELL2 = "CLASS";
        final String TITLE_CELL3 = "NOTES";

        String[][] content = new String[][]
                {
                        extra.norwegianWords,
                        extra.englishWordClass, extra.englishWordNotes,
                        extra.persianWordClass, extra.persianWordNotes,
                        extra.arabicWordClass, extra.arabicWordNotes
                };

        makeTable(extra.getWordCount(), TITLE_CELL1, TITLE_CELL2, TITLE_CELL3, content);
    }

    public void displaySentenceStructure(View view)
    {
        showing_information.setText(str_structure);
        showing_information_type = 3;

        isOtherInformationTable(false);
    }

    private void makeTable(int wordCount, String cellTitle1, String cellTitle2, String cellTitle3, String[][] DB_Content)
    {
        final int WORD_CELL = 0;
        final int SECOND_CELL = 1;
        final int THIRD_CELL = 2;
        final int NUM_ROW_ITEMS = 3;

        String[] temp;
        TableRow tableRow;
        TextView table_item;

        for (int i = 0; i < wordCount + 1; i++) // +1 to accomodate column title row
        {
            tableRow = new TableRow(this);

            //region Applying stylistic TableRow attributes
            TableLayout.LayoutParams tableLayoutParams;
            tableLayoutParams = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            tableLayoutParams.setMargins(SMV(20), 0, SMV(20), 0);
            tableRow.setLayoutParams(tableLayoutParams);
            tableRow.setGravity(View.TEXT_ALIGNMENT_CENTER);
            //endregion

            for (int j = 0; j < NUM_ROW_ITEMS; j++)
            {
                table_item = new TextView(this);

                //region Applying stylistic TextView
                TableRow.LayoutParams rowLayoutParams;
                rowLayoutParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                table_item.setLayoutParams(rowLayoutParams);
                table_item.setTextColor(Color.parseColor("#4d8e11"));
                table_item.setTextSize(TextSMV(40));
                table_item.setGravity(View.TEXT_ALIGNMENT_CENTER);
                table_item.setPadding(SMV(20), SMV(13), SMV(20), SMV(13));
                //endregion

                tableRow.addView(table_item);
            }
            table.addView(tableRow);
        }

        //region Sets up column title row
        TableRow columnTitleRow = (TableRow) table.getChildAt(0);
        ((TextView)columnTitleRow.getChildAt(WORD_CELL)).setText(cellTitle1);
        ((TextView)columnTitleRow.getChildAt(WORD_CELL)).setTextSize(TextSMV(50));
        ((TextView)columnTitleRow.getChildAt(SECOND_CELL)).setText(cellTitle2);
        ((TextView)columnTitleRow.getChildAt(SECOND_CELL)).setTextSize(TextSMV(50));
        ((TextView)columnTitleRow.getChildAt(THIRD_CELL)).setText(cellTitle3);
        ((TextView)columnTitleRow.getChildAt(THIRD_CELL)).setTextSize(TextSMV(50));
        //endregion

        // Fill in cells with norwegian words
        temp = DB_Content[0];
        for (int i = 0; i < wordCount; i++)
        {
            TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
            TextView tempTextView = (TextView) tempRow.getChildAt(WORD_CELL);
            tempTextView.setText(temp[i]);
        }

        //region Sets correctly translated text
        switch (some_language)
        {
            case 0:
                // Fill in cells with the norwegian words' meaning in English
                temp = DB_Content[1];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(SECOND_CELL);
                    tempTextView.setText(temp[i]);
                }
                // Fill in cells with the norwegian words' function in English
                temp = DB_Content[2];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(THIRD_CELL);
                    tempTextView.setText(temp[i]);
                }
                break;
            case 1:
                // Fill in cells with the norwegian words' meaning in Persian
                temp = DB_Content[3];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(SECOND_CELL);
                    tempTextView.setText(temp[i]);
                }
                // Fill in cells with the norwegian words' function in Persian
                temp = DB_Content[4];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(THIRD_CELL);
                    tempTextView.setText(temp[i]);
                }
                break;
            case 2:
                // Fill in cells with the norwegian words' meaning in Arabic
                temp = DB_Content[5];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(SECOND_CELL);
                    tempTextView.setText(temp[i]);
                }
                // Fill in cells with the norwegian words' function in Persian
                temp = DB_Content[6];
                for (int i = 0; i < wordCount; i++)
                {
                    TableRow tempRow = (TableRow) table.getChildAt(i + 1); // +1 to skip column title row
                    TextView tempTextView = (TextView) tempRow.getChildAt(THIRD_CELL);
                    tempTextView.setText(temp[i]);
                }
                break;
        }
        //endregion

        Toast.makeText(this, DB_Content[0][0], Toast.LENGTH_SHORT).show();
    }

    public void goto_Language_Missions(View view)
    {
        Intent lang_intent = new Intent(this, amiciandoskar.jsnorway.language_missions.class);
        lang_intent.putExtra("amiciandoskar.jsnorway.CURRENT_ITEM", index_current_language_item);
        lang_intent.putExtra("amiciandoskar.jsnorway.CURRENT_LANG", some_language);
        lang_intent.putExtra("amiciandoskar.jsnorway.PRIMARY_OR_ALT", toggleAlternativeSentence);
        startActivity(lang_intent);
    }

    public void makeEnglish(View view)
    {
        setLanguage(0);
        preferences_editor.putInt("language_setting", 0);
        preferences_editor.apply();
    }

    public void makePersian(View view)
    {
        setLanguage(1);
        preferences_editor.putInt("language_setting", 1);
        preferences_editor.apply();
    }

    public void makeArabic(View view)
    {
        setLanguage(2);
        preferences_editor.putInt("language_setting", 2);
        preferences_editor.apply();
    }

    private void changeLanguageStrings(int lang)
    {
        switch (lang)
        {
            case 0:
                str_primary = eng_str_primary;
                str_alt = eng_str_alt;
                str_translation = eng_str_translation;
                str_explanation = eng_str_explanation;
                str_extra = eng_str_extra;
                str_structure = eng_str_structure;
                break;
            case 1:
                str_primary = pers_str_primary;
                str_alt = pers_str_alt;
                str_translation = pers_str_translation;
                str_explanation = pers_str_explanation;
                str_extra = pers_str_extra;
                str_structure = pers_str_structure;
                break;
            case 2:
                str_primary = arab_str_primary;
                str_alt = arab_str_alt;
                str_translation = arab_str_translation;
                str_explanation = arab_str_explanation;
                str_extra = arab_str_extra;
                str_structure = arab_str_structure;
                break;
        }

        String sentenceReminderToApply = (toggleAlternativeSentence) ?
                str_alt : str_primary;
        showing_sentence.setText(sentenceReminderToApply);
    }

    private void setLanguage(int lang)
    {
        switch (lang)
        {
            case 0:
                some_language = 0;
                break;
            case 1:
                some_language = 1;
                break;
            case 2:
                some_language = 2;
                break;
        }

        changeLanguageStrings(some_language);
        updateShowingInformation(showing_information_type);
    }

    private void updateShowingInformation(int typeShown)
    {
        switch (typeShown)
        {
            case 0:
                displayTranslation(null);
                break;
            case 1:
                displayExplanationByWord(null);
                break;
            case 2:
                displayExtraExplanation(null);
                break;
            case 3:
                displaySentenceStructure(null);
                break;
        }
    }
}
