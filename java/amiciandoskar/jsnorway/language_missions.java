package amiciandoskar.jsnorway;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import static java.lang.String.valueOf;

public class language_missions extends AppCompatActivity
{
    Button english, persian, arabic, info;
    TextView language_item_num;
    Button challenge_completed, know_sentence;

    TextView norwegian_sentence, mission_description;
    TextView showing_sentence, showing_mission_description;

    RadioGroup radio_group_challenge;
    RadioButton radio_1, radio_2, radio_3,
                radio_4, radio_5, radio_6,
                radio_7, radio_8, radio_9;

    LinearLayout points;
    TextView points_1, points_2, points_3,
             points_4, points_5, points_6,
             points_7, points_8, points_9;

    SharedPreferences app_preferences;
    SharedPreferences.Editor preferences_editor;

    int some_language;
    int index_current_language_item;
    int LAST_LANGUAGE_ITEM;         // initialized with helper.getNumOfLanguageItems()
    int index_current_mission = 1;

    final String txtA = "lang_item", txtB = "challengeCompleted";
    final String colorCompleted = "#002a6d", colorNotCompleted = "#d6d7d7";

    JSNorwaySQLHelper.Mission current_mission = null;

    float screenSizeModifier;       // initialized with initializeModifier()

    boolean toggleAlternativeSentence = false;

    DisplayMetrics metrics;

    JSNorwaySQLHelper helper = new JSNorwaySQLHelper(this);

    JSNorwaySQLHelper.LanguageItem current_language_item;

    //region GUI Strings
    // GUI String variables
    String str_primary;
    String str_alt;
    String str_showing_mission_description;
    // English GUI Strings
    static final String eng_str_primary = "Primary Sentence to Learn";
    static final String eng_str_alt = "Alternative Sentence";
    static final String eng_str_showing_mission_description = "Showing Mission: ";

    // Persian GUI Strings
    static final String pers_str_primary = "حکم اولیه برای یادگیری";
    static final String pers_str_alt = "حکم جایگزین";
    static final String pers_str_showing_mission_description = "نمایش ماموریت: ";


    // Arabic GUI Strings
    static final String arab_str_primary = "الجملة الابتدائية لتعلم";
    static final String arab_str_alt = "الجملة البديلة";
    static final String arab_str_showing_mission_description = "عرض الرسالة: ";

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_missions);

        //region Sets up settings functionality
        app_preferences = getSharedPreferences("app_settings", 0);
        preferences_editor = app_preferences.edit();
        //endregion

        some_language = getIntent().getIntExtra("amiciandoskar.jsnorway.CURRENT_LANG", 0);
        index_current_language_item = getIntent().getIntExtra("amiciandoskar.jsnorway.CURRENT_ITEM", 0);
        toggleAlternativeSentence = getIntent().getBooleanExtra("amiciandoskar.jsnorway.PRIMARY_OR_ALT", false);

        //region Retrieving views
        // sets up language buttons and info-button
        english = (Button) findViewById(R.id.btn_english);
        persian = (Button) findViewById(R.id.btn_persian);
        arabic = (Button) findViewById(R.id.btn_arabic);
        info = (Button) findViewById(R.id.btn_info);

        // sets up textview for language item number
        language_item_num = (TextView) findViewById(R.id.txt_language_item_num);

        // sets up button for setting challenge as completed
        challenge_completed = (Button) findViewById(R.id.btn_go_to_missions);
        know_sentence = (Button)  findViewById(R.id.btn_know_sentence);

        // sets up textviews for sentence display and challenge description
        showing_sentence = (TextView) findViewById(R.id.showing_sentence);
        norwegian_sentence = (TextView) findViewById(R.id.txt_norwegian_sentence);
        showing_mission_description = (TextView) findViewById(R.id.showing_mission_description);
        mission_description = (TextView) findViewById(R.id.txt_mission_description);

        // sets up radio group for challenges
        radio_group_challenge = (RadioGroup) findViewById(R.id.radio_group_challenge);

        // sets up the radiobuttons for challenges
        radio_1 =  (RadioButton) findViewById(R.id.radio_1);
        radio_2 =  (RadioButton) findViewById(R.id.radio_2);
        radio_3 =  (RadioButton) findViewById(R.id.radio_3);
        radio_4 =  (RadioButton) findViewById(R.id.radio_4);
        radio_5 =  (RadioButton) findViewById(R.id.radio_5);
        radio_6 =  (RadioButton) findViewById(R.id.radio_6);
        radio_7 =  (RadioButton) findViewById(R.id.radio_7);
        radio_8 =  (RadioButton) findViewById(R.id.radio_8);
        radio_9 =  (RadioButton) findViewById(R.id.radio_9);

        // sets up radio group for challenges
        points = (LinearLayout) findViewById(R.id.points);

        // sets up textviews for points worth of each mission
        points_1 =  (TextView) findViewById(R.id.points1);
        points_2 =  (TextView) findViewById(R.id.points2);
        points_3 =  (TextView) findViewById(R.id.points3);
        points_4 =  (TextView) findViewById(R.id.points4);
        points_5 =  (TextView) findViewById(R.id.points5);
        points_6 =  (TextView) findViewById(R.id.points6);
        points_7 =  (TextView) findViewById(R.id.points7);
        points_8 =  (TextView) findViewById(R.id.points8);
        points_9 =  (TextView) findViewById(R.id.points9);
        //endregion

        LAST_LANGUAGE_ITEM = helper.getNumOfLanguageItems();
        initializeModifier();
        modifyScreen();

        // apply language settings
        applyCurrentSentence();
        current_mission = current_language_item.missionsList.missions[0];
        setLanguage(some_language);

        //region set points text
        points_1.setText(valueOf(current_language_item.missionsList.missions[0].pointsWorth));
        points_2.setText(valueOf(current_language_item.missionsList.missions[1].pointsWorth));
        points_3.setText(valueOf(current_language_item.missionsList.missions[2].pointsWorth));
        points_4.setText(valueOf(current_language_item.missionsList.missions[3].pointsWorth));
        points_5.setText(valueOf(current_language_item.missionsList.missions[4].pointsWorth));
        points_6.setText(valueOf(current_language_item.missionsList.missions[5].pointsWorth));
        points_7.setText(valueOf(current_language_item.missionsList.missions[6].pointsWorth));
        points_8.setText(valueOf(current_language_item.missionsList.missions[7].pointsWorth));
        points_9.setText(valueOf(current_language_item.missionsList.missions[8].pointsWorth));
        //endregion

        index_current_mission = 1;
        current_mission = current_language_item.missionsList.missions[index_current_mission-1];
        applySelectedMission(); radio_1.toggle();
        initChallengeCompleted();
    }

    private void initializeModifier()
    {
        metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        screenSizeModifier = 0.001F * metrics.widthPixels;
    }

    // S.M.V. = screenModifiedValue
    private int SMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier);
    }

    // S.M.V. = screenModifiedValue
    private int TextSMV(int originalValue)
    {
        return java.lang.Math.round(originalValue * screenSizeModifier / metrics.scaledDensity);
    }

    // S.M.V. = screenModifiedValue
    private float SMV(float originalValue)
    {
        return originalValue * screenSizeModifier;
    }

    // modifies the screen elements' distance and size values based on screen size in width
    private void modifyScreen()
    {
        //region Language Buttons + Info
        LinearLayout.LayoutParams languageBtnLayoutParams;
        languageBtnLayoutParams = new LinearLayout.LayoutParams(SMV(250), SMV(150));
        int langBtnPadding = SMV(40);
        int langBtnTextSize = TextSMV(40);

        english.setLayoutParams(languageBtnLayoutParams);
        english.setTextSize(langBtnTextSize);
        english.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        persian.setLayoutParams(languageBtnLayoutParams);
        persian.setTextSize(langBtnTextSize);
        persian.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        arabic.setLayoutParams(languageBtnLayoutParams);
        arabic.setTextSize(langBtnTextSize);
        arabic.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);

        info.setLayoutParams(new LinearLayout.LayoutParams(SMV(150), SMV(150)));
        info.setTextSize(langBtnTextSize);
        info.setPadding(langBtnPadding, langBtnPadding, langBtnPadding, langBtnPadding);
        //endregion

        //region Upper Buttons

        //endregion

        //region Lower Buttons

        //endregion
    }

    private void applyCurrentSentence()
    {
        language_item_num.setText(valueOf(index_current_language_item) + "/" + valueOf(LAST_LANGUAGE_ITEM));

        current_language_item = helper.getLanguageItem(index_current_language_item);

        String sentenceToApply = (toggleAlternativeSentence) ?
                current_language_item.alternativeSentence :
                current_language_item.primarySentence;


        // Sets sentence to new primary sentence by default
        norwegian_sentence.setText(sentenceToApply);
    }

    public void setSelectedMission(View view)
    {
        // Checking which radio button was clicked
        switch(view.getId())
        {
            case R.id.radio_1:
                index_current_mission = 1;
                break;
            case R.id.radio_2:
                index_current_mission = 2;
                break;
            case R.id.radio_3:
                index_current_mission = 3;
                break;
            case R.id.radio_4:
                index_current_mission = 4;
                break;
            case R.id.radio_5:
                index_current_mission = 5;
                break;
            case R.id.radio_6:
                index_current_mission = 6;
                break;
            case R.id.radio_7:
                index_current_mission = 7;
                break;
            case R.id.radio_8:
                index_current_mission = 8;
                break;
            case R.id.radio_9:
                index_current_mission = 9;
                break;
        }

        current_mission = current_language_item.missionsList.missions[index_current_mission-1];
        applySelectedMission();
    }

    private void applySelectedMission()
    {
        switch (some_language)
        {
            case 0:
                showing_mission_description.setText(str_showing_mission_description + current_mission.engTypeOfMission);
                mission_description.setText(current_mission.englishDescription);
                break;
            case 1:
                showing_mission_description.setText(str_showing_mission_description + current_mission.persTypeOfMission);
                mission_description.setText(current_mission.persianDescription);
                break;
            case 2:
                showing_mission_description.setText(str_showing_mission_description + current_mission.arabTypeOfMission);
                mission_description.setText(current_mission.arabicDescription);
                break;
            default:
                showing_mission_description.setText(str_showing_mission_description + "failed");
                mission_description.setText("failed to load description");
                break;
        }
    }

    public void makeEnglish(View view)
    {
        setLanguage(0);
        preferences_editor.putInt("language_setting", 0);
        preferences_editor.apply();
    }

    public void makePersian(View view)
    {
        setLanguage(1);
        preferences_editor.putInt("language_setting", 1);
        preferences_editor.apply();
    }

    public void makeArabic(View view)
    {
        setLanguage(2);
        preferences_editor.putInt("language_setting", 2);
        preferences_editor.apply();
    }

    public void knowIt(View view)
    {
        final String txt = "knowNumber";
        String number = Integer.toString(index_current_language_item);

        preferences_editor.putBoolean(txt + number, true);
        finish();
    }

    private void initChallengeCompleted()
    {
        final String numItem = Integer.toString(index_current_language_item);

        int challengeId;
        String txtChallengeId;
        View temp_txt_view;
        RadioButton temp_btn;
        Boolean isCompleted;

        for (int i = 0; i < radio_group_challenge.getChildCount(); i++)
        {
            challengeId = radio_group_challenge.getChildAt(i).getId();
            txtChallengeId = Integer.toString(challengeId);
            temp_txt_view = getComplementaryTxtView(challengeId);
            temp_btn = (RadioButton) findViewById(challengeId);
            isCompleted = app_preferences.getBoolean(txtA + numItem + txtB + txtChallengeId, false);

            if (isCompleted)
            {
                temp_btn.setAlpha(0.8f);
                temp_btn.setBackgroundColor(Color.parseColor(colorCompleted));
                temp_txt_view.setAlpha(0.8f);
                temp_txt_view.setBackgroundColor(Color.parseColor(colorCompleted));
            }
        }
    }

    public void setChallengeCompleted(View view)
    {
        final String numItem = Integer.toString(index_current_language_item);

        int temp_radio_id = radio_group_challenge.getCheckedRadioButtonId();
        View temp_txt_view = getComplementaryTxtView(temp_radio_id);
        RadioButton temp_btn = (RadioButton) findViewById(temp_radio_id);

        String txtChallengeId = Integer.toString(temp_radio_id);

        Boolean isCompleted = app_preferences.getBoolean(txtA + numItem + txtB + txtChallengeId, false);

        if (isCompleted)
        {
            temp_btn.setAlpha(1);
            temp_btn.setBackgroundColor(Color.parseColor(colorNotCompleted));
            temp_txt_view.setAlpha(1);
            temp_txt_view.setBackgroundColor(Color.parseColor(colorNotCompleted));
            preferences_editor.putBoolean(txtA + numItem + txtB + txtChallengeId, false);
            preferences_editor.commit();
        }
        else
        {
            temp_btn.setAlpha(0.8f);
            temp_btn.setBackgroundColor(Color.parseColor(colorCompleted));
            temp_txt_view.setAlpha(0.8f);
            temp_txt_view.setBackgroundColor(Color.parseColor(colorCompleted));
            preferences_editor.putBoolean(txtA + numItem + txtB + txtChallengeId, true);
            preferences_editor.commit();
        }
    }

    // gets the textview next to a radio button
    private View getComplementaryTxtView(int radioId)
    {
        int temp_id;

        for (int i = 0; i < radio_group_challenge.getChildCount(); i++)
        {
            temp_id = radio_group_challenge.getChildAt(i).getId();

            if (radioId == temp_id)
            {
                return points.getChildAt(i);
            }
        }
        return null;
    }

    private void changeLanguageStrings(int lang)
    {
        switch (lang)
        {
            case 0:
                str_primary = eng_str_primary;
                str_alt = eng_str_alt;
                str_showing_mission_description = eng_str_showing_mission_description;
                break;
            case 1:
                str_primary = pers_str_primary;
                str_alt = pers_str_alt;
                str_showing_mission_description = pers_str_showing_mission_description;
                break;
            case 2:
                str_primary = arab_str_primary;
                str_alt = arab_str_alt;
                str_showing_mission_description = arab_str_showing_mission_description;
                break;
        }

        String titleToApply = (toggleAlternativeSentence) ?
                str_alt : str_primary;
        showing_sentence.setText(titleToApply);
    }

    private void setLanguage(int lang)
    {
        switch (lang)
        {
            case 0:
                some_language = 0;
                break;
            case 1:
                some_language = 1;
                break;
            case 2:
                some_language = 2;
                break;
        }

        changeLanguageStrings(some_language);
        applySelectedMission();

        setRadioBtnText(radio_1, 0);
        setRadioBtnText(radio_2, 1);
        setRadioBtnText(radio_3, 2);
        setRadioBtnText(radio_4, 3);
        setRadioBtnText(radio_5, 4);
        setRadioBtnText(radio_6, 5);
        setRadioBtnText(radio_7, 6);
        setRadioBtnText(radio_8, 7);
        setRadioBtnText(radio_9, 8);
    }

    private void setRadioBtnText(RadioButton rBtn, int index)
    {
        switch (some_language)
        {
            case 0:
                rBtn.setText(current_language_item.missionsList.missions[index].engTypeOfMission);
                break;
            case 1:
                rBtn.setText(current_language_item.missionsList.missions[index].persTypeOfMission);
                break;
            case 2:
                rBtn.setText(current_language_item.missionsList.missions[index].arabTypeOfMission);
                break;
        }
    }
}